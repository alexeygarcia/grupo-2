import { Controller, Delete, Get } from '@nestjs/common';
import { get } from 'http';

@Controller('usuario')
export class UsuarioController {
    @Get('obtener-usuarios') // http://localhost:3000/usuario/obtener-usuarios
    obtenerUsuarios(){
        return {
            mensaje: 'Todos los usuarios'
        }
    }
}
